USE pruebas;
# Listar el login de los usuarios con nivel 1 o 3
SELECT t.usuario FROM tblusuarios t WHERE t.nivel=1 OR t.nivel=3;

# Listar nombre y teléfono de los usuarios con teléfono que no sea de la marca BLACKBERRY 
SELECT t.nombre, t.telefono FROM tblusuarios t WHERE t.marca!='BLACKBERRY';

# Listar el login de los usuarios con nivel 3
SELECT t.usuario FROM tblusuarios t WHERE t.nivel=3;

# Listar el login de los usuarios con nivel 0
SELECT t.usuario FROM tblusuarios t WHERE t.nivel=0;

# Listar el login de los usuarios con nivel 1
SELECT t.usuario FROM tblusuarios t WHERE t.nivel=1;

# Contar el número de usuarios por sexo
SELECT t.sexo, COUNT(*) FROM tblusuarios t GROUP BY t.sexo;

# Listar el login y teléfono de los usuarios con compañia telefónica AT&T
SELECT t.usuario, t.telefono FROM tblusuarios t WHERE t.compañia='AT&T';

# Listar las diferentes compañias en orden alfabético descendentemente
SELECT DISTINCT t.compañia FROM tblusuarios t ORDER BY t.compañia DESC;

# Listar el login de los usuarios inactivos
SELECT t.usuario FROM tblusuarios t WHERE t.activo=0;

# Listar los números de teléfono sin saldo
SELECT t.telefono FROM tblusuarios t WHERE t.saldo=0;

# Calcular el saldo mínimo de los usuarios de sexo "Hombre"
SELECT MIN(t.saldo) FROM tblusuarios t WHERE t.sexo='h';

# Listar los números de teléfono con saldo mayor a 300
SELECT t.telefono FROM tblusuarios t WHERE t.saldo>300;