﻿CREATE DATABASE agenda;
USE agenda;

CREATE TABLE IF NOT EXISTS contactos(
  numero_contacto int,
  nombre varchar(20),
  apellidos varchar(40),
  fecha_nac date,
  telefono varchar(9) NOT NULL DEFAULT(000000000),
  PRIMARY KEY(numero_contacto)
  );

CREATE TABLE IF NOT EXISTS clientes(
  codigo_cliente int,
  nombre varchar(20),
  apellidos varchar(40),
  dni varchar(10),
  descuento int DEFAULT(5),
  tarjeta_socio boolean,
  PRIMARY KEY (codigo_cliente)
  );

INSERT INTO contactos(numero_contacto, nombre, apellidos, fecha_nac, telefono)
  VALUES(1, 'Antonio', 'García Pérez', '1960/08/15', '942369521'),
  (2, 'Carlos', 'Pérez Ruiz', '1958/04/26', '942245147'),
  (3, 'Luis', 'Rodrígez Más', '1961/03/30', '942296578'),
  (4, 'Jaime', 'Juangrán Sornes', '1968/01/31', '942368496'),
  (5, 'Alfonso', 'Prats Montolla', '1969/04/28', '942354852'),
  (6, 'José', 'Navarro Lard', '1964/05/15', '942387469'),
  (7, 'Elisa', 'Úbeda Sansón', '1962/07/10', '942357812'),
  (8, 'Eva', 'San Martin', '1965/08/12', '942241589'),
  (9, 'Juan', 'García Gómez', '1974/05/15', '942359887');

INSERT INTO clientes (codigo_cliente, nombre, apellidos, dni, descuento, tarjeta_socio)
  VALUES (1, 'Gerardo', 'Hernández Luis', '11111111A', 5, 1),
  (2, 'Carlos', 'Prats Ruiz', '22222222B', 5, 0),
  (3, 'Lourdes', 'Oliver Peris', '33333333C', 10, 1),
  (4, 'Sergio', 'Larred Navas', '44444444D', 15, 1),
  (5, 'Joaquín', 'Árboles Onsins', '55555555E', 5, 0),
  (6, 'Antonio', 'Aznar Zapatero', '66666666F', 10, 1);