﻿USE tienda;
#1.	Lista el nombre de todos los productos que hay en la tabla producto.
SELECT p.nombre FROM producto p;

#2.	Lista los nombres y los precios de todos los productos de la tabla producto.
SELECT p.nombre, p.precio FROM producto p;

#3.	Lista todas las columnas de la tabla producto.
SELECT * FROM producto p;

#4 Lista el nombre de los productos, el precio en euros y el precio en dólares estadounidenses (USD).
SELECT p.nombre, p.precio, p.precio*1.5 AS dolar FROM producto p;

#5.	Lista el nombre de los productos, el precio en euros y el precio en dólares estadounidenses (USD). Utiliza los siguientes alias para las columnas: nombre de producto, euros, dólares.
SELECT p.nombre AS nombre, p.precio AS euro, p.precio*1.5 AS dolar FROM producto p;

#6.	Lista los nombres y los precios de todos los productos de la tabla producto, convirtiendo los nombres a mayúscula.
SELECT UPPER(p.nombre), p.precio FROM producto p; 

#7.	Lista los nombres y los precios de todos los productos de la tabla producto, convirtiendo los nombres a minúscula.
SELECT LOWER(p.nombre), p.precio FROM producto p;

#9.	Lista los nombres y los precios de todos los productos de la tabla producto, redondeando el valor del precio.
SELECT p.nombre, ROUND(p.precio) FROM producto p;

#10.	Lista los nombres y los precios de todos los productos de la tabla producto, truncando el valor del precio para mostrarlo sin ninguna cifra decimal.
SELECT p.nombre, TRUNCATE(p.precio,0) FROM producto p;

#11.	Lista el código de los fabricantes que tienen productos en la tabla producto.
SELECT f.codigo FROM fabricante f INNER JOIN producto p ON f.codigo = p.codigo_fabricante;

#12.	Lista el código de los fabricantes que tienen productos en la tabla producto, eliminando los códigos que aparecen repetidos.
SELECT DISTINCT f.codigo FROM fabricante f INNER JOIN producto p ON f.codigo = p.codigo_fabricante;

#13.	Lista los nombres de los fabricantes ordenados de forma ascendente
SELECT f.nombre FROM fabricante f ORDER BY f.nombre ASC;

#14.	Lista los nombres de los fabricantes ordenados de forma descendente.
SELECT f.nombre FROM fabricante f ORDER BY f.nombre DESC;

#15.	Lista los nombres de los productos ordenados en primer lugar por el nombre de forma ascendente y en segundo lugar por el precio de forma descendente.
SELECT p.nombre FROM producto p ORDER BY p.nombre ASC, p.precio DESC;

#16.	Devuelve una lista con las 5 primeras filas de la tabla fabricante. (LIMIT 5)
SELECT * FROM fabricante f LIMIT 5;

#18.	Lista el nombre y el precio del producto más barato. (Utilice solamente las cláusulas ORDER BY y LIMIT)
SELECT p.nombre, p.precio FROM producto p ORDER BY p.precio; # SIN HACER

#19.	Lista el nombre y el precio del producto más caro. (Utilice solamente las cláusulas ORDER BY y LIMIT)
SELECT p.nombre, p.precio FROM producto p ORDER BY p.precio; # SIN HACER

#20.	Lista el nombre de todos los productos del fabricante cuyo código de fabricante es igual a 2.
SELECT p.nombre FROM producto p INNER JOIN fabricante f ON p.codigo_fabricante = f.codigo WHERE f.codigo=2;

#21.	Lista el nombre de los productos que tienen un precio menor o igual a 120€.
SELECT p.nombre FROM producto p WHERE p.precio<=120;

#22.	Lista el nombre de los productos que tienen un precio mayor o igual a 400€.
SELECT p.nombre FROM producto p WHERE p.precio>=400;

#24.	Lista todos los productos que tengan un precio entre 80€ y 300€. Sin utilizar el operador BETWEEN.
SELECT * FROM producto p WHERE p.precio BETWEEN 80 AND 300;

#25.	Lista todos los productos que tengan un precio entre 60€ y 200€. Utilizando el operador BETWEEN.
SELECT * FROM producto p WHERE p.precio BETWEEN 60 AND 200;

#26.	Lista todos los productos que tengan un precio mayor que 200€ y que el código de fabricante sea igual a 6.
SELECT * FROM producto p  WHERE p.precio>200 AND p.codigo_fabricante=6;

#28.	Lista todos los productos donde el código de fabricante sea 1, 3 o 5. Utilizando el operador IN.
SELECT * FROM producto p WHERE p.codigo_fabricante IN(1, 3, 5);

#29.	Lista el nombre y el precio de los productos en céntimos (Habrá que multiplicar por 100 el valor del precio). Cree un alias para la columna que contiene el precio que se llame céntimos.
SELECT p.nombre, p.precio, p.precio*100 AS centimos FROM producto p; #SIN ENTENDER

#30.	Lista los nombres de los fabricantes cuyo nombre empiece por la letra S.
SELECT f.nombre FROM fabricante f WHERE f.nombre LIKE 's%';

#31.	Lista los nombres de los fabricantes cuyo nombre termine por la vocal e.
SELECT f.nombre FROM fabricante f WHERE f.nombre LIKE '%e';

#32.	Lista los nombres de los fabricantes cuyo nombre contenga el carácter w.
SELECT f.nombre FROM fabricante f WHERE f.nombre LIKE '%w%';

#34.	Devuelve una lista con el nombre de todos los productos que contienen la cadena Portátil en el nombre.
SELECT p.nombre FROM producto p WHERE p.nombre LIKE '%portatil%';

#35.	Devuelve una lista con el nombre de todos los productos que contienen la cadena Monitor en el nombre y tienen un precio inferior a 215 €.
SELECT p.nombre FROM producto p WHERE p.nombre LIKE '%monitor%' AND p.precio<215;

#36.	Lista el nombre y el precio de todos los productos que tengan un precio mayor o igual a 180€. Ordene el resultado en primer lugar por el precio (en orden descendente) y en segundo lugar por el nombre (en orden ascendente).
SELECT p.nombre, p.precio FROM producto p WHERE p.precio>=180 ORDER BY p.precio DESC, p.nombre ASC;