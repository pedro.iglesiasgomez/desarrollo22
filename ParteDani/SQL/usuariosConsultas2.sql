USE pruebas;
# Listar nombre y teléfono de los usuarios con teléfono que no sea de la marca LG o SAMSUNG
SELECT t.nombre, t.telefono FROM tblusuarios t WHERE t.marca!='lg' AND t.marca!='SAMSUNG';

# Listar el login y teléfono de los usuarios con compañia telefónica IUSACELL
SELECT t.usuario, t.telefono FROM tblusuarios t WHERE t.compañia='iusacell';

# Listar el login y teléfono de los usuarios con compañia telefónica que no sea TELCEL
SELECT t.usuario, t.telefono FROM tblusuarios t WHERE t.compañia!='telcel';

# Calcular el saldo promedio de los usuarios que tienen teléfono marca NOKIA
SELECT AVG(t.saldo) FROM tblusuarios t WHERE t.marca='nokia';

# Listar el login y teléfono de los usuarios con compañia telefónica IUSACELL o AXEL
SELECT t.usuario, t.telefono FROM tblusuarios t WHERE t.compañia='IUSACELL' OR t.compañia='AXEL';

# Mostrar el email de los usuarios que no usan yahoo
SELECT t.email FROM tblusuarios t WHERE t.email NOT LIKE '%yahoo%';

# Listar el login y teléfono de los usuarios con compañia telefónica que no sea TELCEL o IUSACELL
SELECT t.usuario, t.telefono FROM tblusuarios t WHERE t.compañia!='TELCEL' AND t.compañia!='IUSACELL';

# Listar el login y teléfono de los usuarios con compañia telefónica UNEFON
SELECT t.usuario, t.telefono FROM tblusuarios t WHERE t.compañia='unefon';

# Listar las diferentes marcas de celular en orden alfabético descendentemente
SELECT DISTINCT t.marca FROM tblusuarios t ORDER BY t.marca DESC;

# Listar las diferentes compañias en orden alfabético aleatorio
SELECT DISTINCT t.compañia FROM tblusuarios t;

# Listar el login de los usuarios con nivel 0 o 2
SELECT t.usuario FROM tblusuarios t WHERE t.nivel=1 OR t.nivel=2;

# Calcular el saldo promedio de los usuarios que tienen teléfono marca LG
SELECT AVG(t.saldo) FROM tblusuarios t WHERE t.marca='lg';