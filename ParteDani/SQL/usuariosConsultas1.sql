
# Listar los nombres de los usuarios
SELECT nombre FROM tblusuarios;

# Calcular el saldo máximo de los usuarios de sexo "Mujer"
SELECT MAX(saldo) FROM tblusuarios WHERE sexo='m';  

#Listar nombre y teléfono de los usuarios con teléfono NOKIA, BLACKBERRY o SONY
SELECT nombre, telefono FROM tblusuarios WHERE marca='nokia' OR marca='blackberry' OR marca='sony';

#Contar los usuarios sin saldo o inactivos
SELECT COUNT(*) FROM tblusuarios t WHERE t.saldo=0 OR t.activo=0;

#Listar el login de los usuarios con nivel 1, 2 o 3
SELECT t.usuario FROM tblusuarios t WHERE t.nivel=1 OR t.nivel=2 OR t.nivel=3;

#Listar los números de teléfono con saldo menor o igual a 300
SELECT t.telefono FROM tblusuarios t WHERE t.saldo<=300;

#Calcular la suma de los saldos de los usuarios de la compañia telefónica NEXTEL
SELECT SUM(t.saldo) FROM tblusuarios t WHERE t.compañia='nextel';

#Contar el número de usuarios por compañía telefónica
SELECT t.compañia, COUNT(*) FROM tblusuarios t GROUP BY t.compañia;

#Contar el número de usuarios por nivel
SELECT COUNT(t.usuario) FROM tblusuarios t GROUP BY t.nivel;

#Listar el login de los usuarios con nivel 2
SELECT t.usuario FROM tblusuarios t WHERE t.nivel=2;

#Mostrar el email de los usuarios que usan gmail
SELECT t.email FROM tblusuarios t WHERE t.email LIKE '%gmail%';

#Listar nombre y teléfono de los usuarios con teléfono LG, SAMSUNG o MOTOROLA
SELECT t.nombre, t.telefono FROM tblusuarios t WHERE t.marca='lg' OR t.marca='samsung' OR t.marca='motorola';
