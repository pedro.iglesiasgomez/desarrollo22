﻿SELECT titulo FROM peliculas WHERE genero='terror';

SELECT titulo FROM peliculas WHERE genero='policiaca' ORDER BY anno DESC;

SELECT anno, pais FROM peliculas WHERE titulo LIKE 'Rocky%';
UPDATE peliculas SET pais='Estados Unidos' WHERE titulo LIKE 'Rocky%';

SELECT * FROM socios WHERE num_socio='S001';

SELECT nombre,apellidos FROM socios;

SELECT nombre, apellidos FROM socios WHERE direccion LIKE '%ale%';

/* Cuenta el numero de socios*/
SELECT COUNT(*) FROM socios;

/* Cuenta el numero de Dramas*/
SELECT COUNT(codigo_peli) FROM peliculas WHERE genero = 'drama';

/*Mestra todos los generos que tenemos (una vez cada uno)*/
SELECT DISTINCT genero FROM peliculas;