﻿USE pruebas;
# Contar el número de usuarios por marca de teléfono
SELECT t.marca, COUNT(t.marca) FROM tblusuarios t GROUP BY t.marca;

# Listar nombre y teléfono de los usuarios con teléfono que no sea de la marca LG
SELECT t.nombre, t.telefono FROM tblusuarios t WHERE t.marca!='lg';

# Listar las diferentes compañias en orden alfabético ascendentemente
SELECT DISTINCT t.compañia FROM tblusuarios t ORDER BY t.compañia ASC;

# Calcular la suma de los saldos de los usuarios de la compañia telefónica UNEFON
SELECT SUM(t.saldo) FROM tblusuarios t WHERE t.compañia='UNEFON';

# Mostrar el email de los usuarios que usan hotmail
SELECT t.email FROM tblusuarios t WHERE t.email LIKE '%hotmail%';

# Listar los nombres de los usuarios sin saldo o inactivos
SELECT t.nombre FROM tblusuarios t WHERE t.saldo=0 OR t.activo=0;

# Listar el login y teléfono de los usuarios con compañia telefónica IUSACELL o TELCEL
SELECT t.usuario, t.telefono FROM tblusuarios t WHERE t.compañia='IUSACELL' OR t.compañia='TELCEL';

# Listar las diferentes marcas de celular en orden alfabético ascendentemente
SELECT DISTINCT t.marca FROM tblusuarios t ORDER BY t.marca ASC;


# Listar las diferentes marcas de celular en orden alfabético aleatorio
SELECT DISTINCT t.marca FROM tblusuarios t;

# Listar el login y teléfono de los usuarios con compañia telefónica IUSACELL o UNEFON
SELECT t.usuario, t.telefono FROM tblusuarios t WHERE t.compañia='IUSACELL' OR t.compañia='UNEFON';

# Listar nombre y teléfono de los usuarios con teléfono que no sea de la marca MOTOROLA o NOKIA
SELECT t.nombre, t.telefono FROM tblusuarios t WHERE t.marca!='motorola' AND t.marca!='nokia';

# Calcular la suma de los saldos de los usuarios de la compañia telefónica TELCEL
SELECT SUM(t.saldo) FROM tblusuarios t WHERE t.compañia='TELCEL';