﻿CREATE DATABASE IF NOT EXISTS videoClub;
USE videoClub;

CREATE TABLE IF NOT EXISTS peliculas(
  codigo_peli varchar(5),
  titulo varchar(40) NOT NULL,
  genero varchar(15),
  anno int,
  pais varchar(15),
  PRIMARY KEY(codigo_peli)
  );

CREATE TABLE IF NOT EXISTS socios(
  num_socio varchar(5),
  dni varchar(10) NOT NULL,
  nombre varchar(20),
  apellidos varchar(40),
  direccion varchar(50),
  telefono varchar(10),
  fecha_nac date,
  PRIMARY KEY(num_socio),
  UNIQUE KEY(dni)
  );

CREATE TABLE IF NOT EXISTS prestamos(
  cod_prestamos int,
  num_socio varchar(5),
  cod_peli varchar(5),
  PRIMARY KEY(cod_prestamos),
  FOREIGN KEY (num_socio) REFERENCES socios(num_socio),
  FOREIGN KEY (cod_peli) REFERENCES peliculas(codigo_peli)
  );

INSERT INTO peliculas(codigo_peli, titulo, genero, anno)
  VALUES ('P001', 'Rebelion en la Ondas', 'Comedia', 1987),
  ('P002', 'Rocky', 'Drama', 1976),
  ('P003', 'El Exorcista', 'Terror', 1973),
  ('P004', 'Ronin', 'Policiaca', 1998),
  ('P005', 'Rocky II', 'Drama', 1979),
  ('P006', 'Cyrano de Bergaraç', 'Drama', 1990),
  ('P007', 'Rocky III', 'Drama', 1982),
  ('P008', 'El Resplandor', 'Terror', 1980),
  ('P009', 'Sin Perdon', 'Western', 1992);

INSERT INTO socios(num_socio, dni, nombre, apellidos, direccion, telefono, fecha_nac)
  VALUES ('S001','10101010A', 'Fernando', 'Alonso', 'Calle Rueda', '985332211', '1980/06/02'),
  ('S002', '20202020B', 'Elmer', 'Benett', 'Avda Castilla', '915852266', '1985/12/05'),
  ('S003', '30303030C', 'Sofia', 'Loren', 'Plaza Mayor', '942335544', '1903/08/07'),
  ('S004', '40404040D', 'Mar', 'Flores', 'Calle Alegria', '914587895', '1978/05/31'),
  ('S005', '50505050E', 'Tamara', 'Torres', 'Calle Perejil', '935784578', '1975/03/24'),
  ('S006', '11111111F', 'Federico', 'Trillo', 'Calle Honduras', '942225544', '1950/04/29'),
  ('S007', '11223344G', 'Marina', 'Castaño', 'Avda Jeta', '916589878', '1957/01/15'),
  ('S008', '72727272H', 'Diego', 'Tristan', 'Calle Galicia', '942353535', '1979/10/19'),
  ('S009', '25252525J', 'Andrei', 'Shevchenko', 'Calle Milan', '915544545', '1982/03/26');

INSERT INTO prestamos(cod_prestamos, num_socio, cod_peli)
  VALUES (001, 'S001', 'P001'),
  (002,'S002', 'P003'),
  (003,'S003', 'P004'),
  (004,'S005', 'P006'),
  (005,'S007', 'P001'),
  (006,'S008', 'P002'),
  (007,'S008', 'P003'),
  (008,'S009', 'P001'),
  (009,'S004', 'P005'),
  (010,'S006', 'P007'),
  (011,'S001', 'P008'),
  (012,'S002', 'P009')
  