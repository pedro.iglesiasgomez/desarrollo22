﻿CREATE DATABASE IF NOT EXISTS mibasededatos;
USE mibasededatos;

CREATE TABLE IF NOT EXISTS tciudad(
  idciudad varchar(3),
  nombre varchar(30),
  provincia varchar(30),
  PRIMARY KEY (idciudad)
  );

CREATE TABLE IF NOT EXISTS mitabla(
  cod int,
  nombre varchar(30),
  fechanac date,
  ciudad varchar(3) NOT NULL,
  PRIMARY KEY (cod),
  FOREIGN KEY(ciudad) REFERENCES tciudad(idciudad)
  );

INSERT INTO tciudad (idciudad, nombre, provincia) VALUES ('1','Santander','Cantabria');
INSERT INTO tciudad (idciudad, nombre, provincia) VALUES ('2','Oviedo','Asturias');
INSERT INTO tciudad (idciudad, nombre, provincia) VALUES ('3','Laredo','Cantabria');
INSERT INTO tciudad (idciudad, nombre, provincia) VALUES ('4','Comillas','Cantabria');
INSERT INTO tciudad (idciudad, nombre, provincia) VALUES ('5','Noja','Cantabria');

INSERT INTO mitabla (cod, nombre, fechanac, ciudad)
 VALUES (1,'Pepe','1986-05-25','1'),
 (2,'Juan','1996-07-05','1'),
 (3,'Pepo','1976-12-25','3'),
 (4,'Lola','1996-04-05','1'),
 (5,'Ana','1999-08-10','1');

SELECT m.cod, m.nombre FROM mitabla m  
  INNER JOIN tciudad t ON t.idciudad = m.ciudad
  WHERE t.provincia!='asturias';