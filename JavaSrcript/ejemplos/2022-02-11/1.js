/*
let promedio = parseInt(prompt("Introduce el promedio"));
let descuento = 0;
let pago = document.querySelector('#dato1').innerHTML;


if(promedio>=9){
    descuento=pago*0.2;
    document.querySelector('#descuento').innerHTML=descuento;
    document.querySelector('#pago').innerHTML=pago-descuento;
} else {
    document.querySelector('#descuento').innerHTML=descuento;
    document.querySelector('#pago').innerHTML=pago-descuento;
}
*/

/**
 * OTRA FORMA DE HACER
 */

let promedio=0; //nota alumno
let descuento=0; //descuento en moneda 
let pago=0; // pago en moneda

/**
 * crear variables que apuntan a los span
 */

let dato1=null;
let cDescuento=null;
let cPago=null;

/**
 * constantes a utilizar
 */

const INSCRIPCION=3000;
const DESCUENTO=0.2;

/**
 * asigno las variables a los span
 */

dato1=document.querySelector('#dato1');
cDescuento=document.querySelector('#descuento');
cPago=document.querySelector('#pago');

/**
 * pedir nota del alumno
 */

promedio = parseInt(prompt("Introduce el promedio"));

if (promedio>=9) {
    descuento=INSCRIPCION*DESCUENTO;   
}


pago=INSCRIPCION-descuento;

/**
 * mostrar los datos
 */

cDescuento.innerHTML=descuento;
cPago.innerHTML=pago;

