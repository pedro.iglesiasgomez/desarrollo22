    let alumnos=[];

    document.querySelector('#borra').addEventListener("click", ()=>{
        alumnos=[];
        localStorage.removeItem("kalumnos");
    });

    document.querySelector('#mas').addEventListener("click",()=>{
        let alunno={
            nombre: document.querySelector('#nombre').value,
            nif: document.querySelector('#nif').value,
            edad: document.querySelector('#edad').value,
        };
        if(localStorage.getItem("kalumnos")==null){
            localStorage.setItem("kalumnos",JSON.stringify(alumnos));
        }else{
            alumnos = JSON.parse(localStorage.getItem("kalumnos"));
        }
    
        alumnos.push(alunno);
        localStorage.setItem("kalumnos", JSON.stringify(alumnos));
        
})