window.addEventListener("load", () => {
    if (localStorage.getItem("dato") == null) {
        datos = [];
        localStorage.setItem("dato", JSON.stringify(datos));
    } else {
        datos = JSON.parse(localStorage.getItem("dato"));
    }


    datos.forEach(e => {
        document.querySelector('div').innerHTML += `<li>Nombre: ${e.nombre}</li>`
        document.querySelector('div').innerHTML += `<li>Edad: ${e.edad}</li>`
    });

    document.querySelector('#borra').addEventListener("click", () => {
        localStorage.removeItem("dato");
        document.querySelector('div').innerHTML = "";
    });


    document.querySelector('#mas').addEventListener("click", () => {
        let texto = {
            nombre: document.querySelector('#nombre').value,
            edad: document.querySelector('#edad').value
        }

        datos = JSON.parse(localStorage.getItem("dato"));
        datos.push(texto);
        localStorage.setItem("dato", JSON.stringify(datos));
        document.querySelector('div').innerHTML += `<li>Nombre: ${texto.nombre}</li>`
        document.querySelector('div').innerHTML += `<li>Edad: ${texto.edad}</li>`
    })
})