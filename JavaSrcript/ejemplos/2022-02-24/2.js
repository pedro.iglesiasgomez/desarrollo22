function sumar() {
    let dato1 = document.querySelector('#inum1');
    let dato2 = document.querySelector('#inum2');

    let suma = parseInt(dato1.value) + parseInt(dato2.value);

    document.querySelector('#resultado').value=suma;
}

function restar() {
    let dato1 = document.querySelector('#inum1');
    let dato2 = document.querySelector('#inum2');

    let resta = parseInt(dato1.value) - parseInt(dato2.value);

    document.querySelector('#resultado').value=resta;

}

/**
 * OTRA OPCION CON TRES FUNCIONES
 */

/*
// creo dos variable globales que apuntan a las cajas

 let dato1 = document.querySelector('#inum1');
 let dato2 = document.querySelector('#inum2');

 function sumar() {
    // creo una variable local para la funcion suma
    let suma = parseInt(dato1.value) + parseInt(dato2.value);

    // llamo al fucion salida para que muestre el resultado
    salida(suma);

}

function restar() {
    let resta = parseInt(dato1.value) - parseInt(dato2.value);
    salida(resta);

}

function salida(valor) {
    let resulatdo = document.querySelector('#resultado');
    resulatdo.value=valor
}
*/
