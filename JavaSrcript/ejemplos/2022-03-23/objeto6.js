/*
Clase llamada persona con los miembros 
nombre
edad
localidad
*/

class Persona {
    constructor(){
        this.nombre="";
        this.edad=0;
        this.localidad="";
    }
};



/*const Persona = function() {
    this.constructor=function(){
        this.nombre="";
        this.edad=0;
        this.localidad="";
    }
    this.constructor();
}*/

const personas = [];
personas.push(new Persona(),new Persona());

personas[0].nombre="Ana Gonzalez";
personas[1].nombre="Luis Gomez";
