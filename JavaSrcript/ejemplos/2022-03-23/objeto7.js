/*
Clase llamada persona con los miembros 
nombre
edad
localidad
*/

class Persona {
    constructor(nombre="",edad=0,localidad=""){
        this.nombre=nombre;
        this.edad=edad;
        this.localidad=localidad;
    }
};

/*const Persona = function(nombre="",edad=0,localidad="") {
    this.constructor=function(nombre,edad,localidad){
        this.nombre=nombre;
        this.edad=edad;
        this.localidad=localidad;
    }
    this.constructor(nombre,edad,localidad);
}*/

const personas = [];
personas.push(
    new Persona(),
    new Persona("Ana Gonzalez"),
    new Persona("Luis Gomez",45)
    );

