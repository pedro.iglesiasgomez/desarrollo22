// creando una clase tipo loro
let Loro = function(){

    // metodo constructor
    this.constructor=function(){
        // creando propiedades publicas
        this.color="rojo";
        this.peso="10";
        console.log("crando loro");
    }

    // creamos metodos publicos
    this.hablar=function(){
        return "piopio";
    }
    this.volar=function(){
        return "volando voy";
    }

    // llamando al constructoe
    this.constructor();
};

/**
 * creo dos objetos de tipo Loro
 */

let loro1 = new Loro();
let loro2 = new Loro();

loro1.color="Azul";