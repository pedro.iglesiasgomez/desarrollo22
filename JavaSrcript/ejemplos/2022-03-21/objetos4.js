// creando una clase tipo loro
class Loro{
    
    constructor(){
        // creando propiedades publicas
        this.color="rojo";
        this.peso="10";
        console.log("creando loro");
    }
    // creamos metodos publicos
    hablar(){
        return "piopio";
    }
    volar(){
        return "volando voy";
    }
};

/**
 * creo dos objetos de tipo Loro
 */

let loro1 = new Loro();
let loro2 = new Loro();

loro1.color="Azul";