/**
 * Crear una clase denominada Perro
 * Perro1 nomenclatura antigua
 * Perro2 nomenclatura moderna (ESS)
 */

/**
 * propiedades
 * 
 * color
 * pelo
 * peso
 * fechaNacimento
 * 
 */

/**
 * Metodos
 * 
 * ladrar (guau guau);
 * dormir ("durmiendo")
 * 
 */

let Perro1 = function(){
    // metodo constructor
    this.constructor=function(){
        this.color="marron";
        this.pelo=true;
        this.peso=25;
        this.fechaNacimiento="15/09/2015";
    }

    // metodos
    this.ladrar=function(){
        return "guau guau";
    }

    this.dormir=function(){
        return "durmiendo";
    }

    this.constructor();
}


class Perro2 {
    constructor(){
        this.color="negro";
        this.pelo=true;
        this.peso=10;
        this.fechaNacimiento="21/03/2022";
    }

    ladrar(){
        return "gua gua";
    }

    dormir(){
        return "ZZZZZZ";
    }
}

// Creamos un ojeto de tipo Perro1 y Perro2
let perrete = new Perro1();
let chucho = new Perro2();

