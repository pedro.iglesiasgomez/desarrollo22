/*
    Crear un objeto con los siguientes miembros
    objeto loro
        MIEMBROS
            porpiedades
                color
                peso
            metodos
                hablar
                volar
*/

// NOTACION JSON

let loro = {
    color: "azul",
    peso: 10,
    
    hablar:()=>{
        return "piopio";
    },

    volar:()=>{
        return "volando voy";
    },
};

// cuidado
let loro1 = loro; // estamos pasando el loro por referencia

/*
lo que cambie a loro afecta a loro1 y
viceversa
*/

loro.plumas=true;

console.log(loro1.plumas);