function calcular() {
    // coloco el nombre escrito
    let dato = null;

    // coloco el numero de vocales
    let contador=0;

    dato = document.querySelector('#inombre').value;

    // pongo dato en minusculas siempre
    dato = dato.toLowerCase();
    
    // para contar el numero de vocales
    for ( let c = 0; c < dato.length; c++){
        if (dato[c]=='a' || 
            dato[c]=='e' ||   
            dato[c]=='i' ||
            dato[c]=='o' ||  
            dato[c]=='u'
        ){
            contador++;
        }
        
        /*switch (dato[c]) {
            case 'a':  
            case 'e':  
            case 'i':  
            case 'o':  
            case 'u':
                contador++;        
        }*/
    }
    
    // escribo el numero de vocales en el input
    document.querySelector('#ivocales').value=contador;

}