class Datos{
    constructor(id=0,nombre="",edad=0,poblacion="",email=""){
        this.id=id;        
        this.nombre=nombre;
        this.edad=edad;
        this.poblacion=poblacion;
        this.email=email;
    }

    imprimir(etiqueta){
        let resultado="";
        resultado +=
        `<${etiqueta}>${this.id}</${etiqueta}>
         <${etiqueta}>${this.nombre}</${etiqueta}>
         <${etiqueta}>${this.edad}</${etiqueta}>
         <${etiqueta}>${this.poblacion}</${etiqueta}>
         <${etiqueta}>${this.email}</${etiqueta}>`;

        return resultado;
    }

    static cabeceras(){
        let resultado="";
        let etiqueta= 'td';
       /* resultado +=
        `<${etiqueta}>id</${etiqueta}>
        <${etiqueta}>nombre</${etiqueta}>
        <${etiqueta}>edad</${etiqueta}>
        <${etiqueta}>poblacion</${etiqueta}>
        <${etiqueta}>email</${etiqueta}>`;
        
        return resultado;*/

        for (let titulo in new this()) {
            resultado += `<td>${titulo}</td>`
        }

        return resultado;
    }

    
}

let personas= [];

/*function cabeceras(objeto){
    let resultado="";
    for (let titulo in objeto) {
        resultado+=`<td>${titulo}</td>`
    }
    return resultado
}*/

let cabeza = Datos.cabeceras();
document.querySelector('thead>tr').innerHTML=cabeza;

document.querySelector('#mas').addEventListener("click",()=>{
    
    /*let datos ={
        edad: null,  
        mail: null, 
        id: null, 
        nombre: null, 
        poblacion: null, 
    };*/
    
    let dato = new Datos(
        document.querySelector('#id').value,
        document.querySelector('#nombre').value,
        document.querySelector('#edad').value,
        document.querySelector('#poblacion').value,
        document.querySelector('#email').value,
    );
    

    document.querySelector('tbody').innerHTML+=`<tr>${dato.imprimir('td')}</tr>`;
    
    personas.push(dato);
    console.log(personas);


});