class Caja {
    static borde="1px solid red";
    constructor(ancho = 100, alto = 100) {
        this._ancho = ancho;
        this._alto = alto;
    }
    get ancho() {
        return this._ancho + "px";
    }

    get alto() {
        return this._alto + "px";
    }

    set ancho(valor) {
        this._ancho = valor;
    }

    set alto(valor) {
        this._alto = valor;
    }

    dibujar(donde) {
        let resultado = document.createElement("div");
        resultado.style.width = this.ancho;
        resultado.style.height = this.alto;
        resultado.style.border=Caja.borde;
        resultado.style.backgroundColor = "black";
        if(donde){
            Caja.mostrar(donde,resultado);
        }
        return resultado;
    }

    static mostrar(donde,objeto) {
        donde.appendChild(objeto);
    }

}
const caja1 = new Caja();
caja1.dibujar(document.querySelector(".dos"));
Caja.mostrar(document.body,caja1.dibujar());
Caja.mostrar(document.querySelector(".uno"),caja1.dibujar());
