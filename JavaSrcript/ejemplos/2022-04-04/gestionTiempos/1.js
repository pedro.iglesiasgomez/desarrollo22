class Lienzo{
    #canvas=null;
    #context=null;
    #ancho=0;
    #alto=0;

    constructor(ancho,alto){
        this.#canvas=document.createElement("canvas");
        document.body.appendChild(this.#canvas);
        this.#context=this.#canvas.getContext("2d");

        this.ancho=ancho;
        this.alto=alto;
    }

    set ancho(valor){
        this.#ancho=valor;
        this.#canvas.setAttribute("width",valor);
    }

    set alto(valor){
        this.#alto=valor;
        this.#canvas.setAttribute("heigth",valor);
    }

    get ancho(){
        return this.#ancho;
    }

    get alto(){
        return this.#alto;
    }

    rellenar(color){
        this.#context.fillStyle = color;
    }

    rectangulo(cx,cy,ancho,alto){
        this.#context.fillRect(cx,cy,ancho,alto);
    }

    trazo(){
        this.#context.beginPath();
        this.#context.moveTo(75,50);
        this.#context.lineTo(100,75);
        this.#context.lineTo(100,25);
        this.#context.closePath();
        this.#context.fill();
    }

    cara(x,y){
        this.#context.beginPath();
        this.#context.arc(x+75,y+75,50,0,Math.PI*2,true); // Círculo externo
        this.#context.moveTo(x+110,y+75);
        this.#context.arc(x+75,y+75,35,0,Math.PI,false);   // Boca (contra reloj)
        this.#context.moveTo(x+65,y+65);
        this.#context.arc(x+60,y+65,5,0,Math.PI*2,true);  // Ojo izquierdo
        this.#context.moveTo(x+95,y+65);
        this.#context.arc(x+90,y+65,5,0,Math.PI*2,true);  // Ojo derecho
        this.#context.stroke();
    }
    limpiar(){
        this.#context.clearRect(0,0,this.#ancho,this.#alto);
    }

}

const lienzo1=new Lienzo(400,100);
const lienzo2=new Lienzo(800,800);
lienzo1.rellenar("red");
lienzo1.rectangulo(0,0,10,10);
lienzo2.cara(600,0);

let x=10;
setInterval(()=> {
    lienzo1.limpiar();
    lienzo1.rectangulo(x,0,10,10)  
    lienzo2.limpiar();
    lienzo2.cara(600-x,0);
    x+=10;
},1000);


