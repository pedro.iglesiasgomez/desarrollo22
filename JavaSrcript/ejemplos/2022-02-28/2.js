function escribir(numero) {
    let muestra = document.querySelector('#display'); 

    if (numero=='c'){
        muestra.innerHTML=null;
    }else{
        muestra.innerHTML+=numero;
    }
}

function mover(argumento) {
    let muestra = document.querySelector('#display');
    const PASO = 10;

    let izqui = window.getComputedStyle(muestra).getPropertyValue("left");
    let ariba = window.getComputedStyle(muestra).getPropertyValue("top");

    if(argumento == "derecha"){
        muestra.style.left = parseInt(izqui)+PASO+'px';
    }else if(argumento == 'abajo'){
        muestra.style.top =  parseInt(ariba)+PASO+'px';
    }
}