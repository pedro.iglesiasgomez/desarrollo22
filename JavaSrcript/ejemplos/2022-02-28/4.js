/**
 * funcion que recib un array de numeros y devuelve el producto
 *  de todos los numeros pares
 */
function productoPares(numeros) {
    let resul=1;
    for (let i = 0; i < numeros.length; i++) {
        if((numeros[i]%2)==0){
            resul *= numeros[i];
        }        
    }
    return resul;
}

/**
 * funcion que recibe un array de numero y retorna la suma de todos los
 * numeros imapres del array
 */

function sumaImpares(numeros){
    let resultado=0;
    for (let i = 0; i < numeros.length; i++) {
        if((numeros[i]%2)!=0){
            resultado += numeros[i];
        }        
    }
    return resultado;
}

let posiciones=[1,5,9,8,7,3,2];
let total = 0;

total=productoPares(posiciones);
console.log(total);

total=sumaImpares(posiciones);
console.log(total);