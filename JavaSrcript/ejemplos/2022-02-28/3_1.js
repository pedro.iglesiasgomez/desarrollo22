function suma(numeros) {
    let resultado=0;
    for (let i = 0; i < numeros.length; i++) { 
        if ((numeros[i]%2)==0){
            resultado += numeros[i];
        }
    }
    return resultado;
}

/**
 * fuera de la funcion
 */
let nums=[
    2,5,7,9,6,3,1,4,8
];

let sumar = suma(nums);

document.write(`la suma de los numero pares es ${sumar}`)
