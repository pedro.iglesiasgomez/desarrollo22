let persona ={
    nombre:"Pepe",
    altura:180,
    saludar:function(){
        return "Hola";
    },
}

console.log(persona["nombre"]);
console.log(persona.nombre);

console.log(persona["saludar"]());
console.log(persona.saludar());


// recorre con for in 

for (let miembro in persona) {
    if(typeof(persona[miembro])!='function'){
        console.log(persona[miembro]);
    }else{
        console.log(persona[miembro]());
    }
}