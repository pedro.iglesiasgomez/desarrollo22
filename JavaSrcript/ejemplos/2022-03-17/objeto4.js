/**
 * recibe una serie de numeros
 * {
 * vector1:[2,3,4,5]
 * vector2:[2,4,6,8]
 * salida:"texto"
 * }
 * la funcion me tiene que devolver un objeto
 * de tipo
 * {
 * suma:[4,7,10,13]
 * producto:[4,12,24,40]
 * max:8 el numero mas alto de los do vectores
 * }
 * 
 * me debe mostrar el valor mas grande en el elemento con id con el
 * valor de salida
 */
function operaciones(datos) {
    let resultado={
        suma:[],
        producto:[],
        max:0
    };

    // calculo la suma y el producto
    datos.vector1.forEach((valor, indice) => {
        resultado.suma[indice]=datos.vector1[indice]+datos.vector2[indice];
        resultado.producto[indice]=datos.vector1[indice]*datos.vector2[indice]
    });

    // calcular el maximo
    resultado.max=Math.max(...datos.vector1,...datos.vector2);

    document.querySelector('#'+datos.salida).innerHTML=resultado.max;
    return resultado;
}



let origen={
    vector1:[2,3,4,5],
    vector2:[2,4,6,8],
    salida:"texto"
}

console.log(operaciones(origen));

console.log(operaciones({
    vector1:[5,6,8],
    vector2:[89,2,3],
    salida:"barra"
}));