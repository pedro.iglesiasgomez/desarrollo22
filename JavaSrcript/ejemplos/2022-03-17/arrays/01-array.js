/*
Vamos a crear arrays
*/

// opcion 1
let a=new Array();
/// array vacio

// opcion 2
let b=[];
//array vacio

// opcion 3
let c=new Array(10);
//array de longitud 10

// opcion 4
let d=[10];
// array de longitud 1

// opcion 5
let e=new Array("lunes");
// array de longitud 1

// opcion 6
let f=new Array("lunes","martes");
// array de longitud 2

// opcion 7
let g=["lunes",0,"martes"];
// array de longitud 3







