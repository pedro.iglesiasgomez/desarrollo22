/**
 * la funcion recibe un string
 * la funcion me cuenta cuantas veces se repite cada vocal
 * me retorna un objeto de este tipo
 * {
 * a:0,
 * e:0,
 * i:0,
 * o:0,
 * u:0
 * }
 */
function contar(texto) {
    let vocales={
        a:0,
        e:0,
        i:0,
        o:0,
        u:0
    }
    texto = texto.toLowerCase();
    for (let c=0; c<texto.length; c++){
            switch(texto[c]){
                case 'a':
                    vocales.a++;
                break;
                case 'e':
                    vocales.e++;
                break;
                case 'i':
                    vocales.i++;
                break;
                case 'o':
                    vocales.o++;
                break;
                case 'u':
                    vocales.u++;
                break;
        }    
    }
    return vocales;
}


console.log(contar("Ejemplo de clase"));