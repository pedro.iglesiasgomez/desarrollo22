let cajas = document.querySelectorAll('div');

// utilizamos un for

for(let c=0;c<cajas.length;c++){
    console.log(cajas[c].innerHTML);
}

// utilizamos el for of

for(let valor of cajas) {
    console.log(valor.innerHTML);
}

// utilzamos el for in

for(let indice in cajas){
    console.log(cajas[indice].innerHTML);
}

// utilizamos el foreach

cajas.forEach((valor, indice)=>{
    console.log(valor.innerHTML);
});
