/**
 * recibe un objeto de tipo
 * {
 * color:"red",
 * radio: 40,
 * borde: 2,
 * colorBorde: "blue",
 * x: 15,
 * y: 20 
 * }
 */

function circulo(datos) {
    let caja = document.createElement("div");

    caja.style.left=datos.x + "px";
    caja.style.top=datos.y + "px";
    caja.style.width=datos.radio + "px";
    caja.style.height=datos.radio + "px";
    caja.style.backgroundColor=datos.color;
    caja.style.borderRadius=datos.radio*2 +"px";
    caja.style.position="absolute";
    caja.style.borderWidth=datos.borde+"px";
    caja.style.borderColor=datos.colorBorde;
    caja.style.borderStyle="solid";

    document.body.appendChild(caja);

}

circulo({
    color:'red',
    x:15,
    y:20,
    radio:40,
    borde:2,
    colorBorde:'blue'
});



circulo({
    color:'yellow',
    x:150,
    y:200,
    radio:40,
    borde:4,
    colorBorde:'black'
});