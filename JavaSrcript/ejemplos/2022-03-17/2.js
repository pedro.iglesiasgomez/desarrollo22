let vector = [
    23,
    "ejemplo",
    45,
    8
];

// añador una propiedad al array
vector.color="rojo";

// utilizando for
for (let c = 0; c < vector.length; c++) {
    console.log(vector[c], c);
    
}

// utilizando foreach
vector.forEach((valor,indice)=>{
    console.log(valor, indice);
});

//utilizando for of
for (let [indice,valor] of vector.entries()) {
    console.log(valor, indice);
}

// utilizando for in
for (let indice in vector){
    console.log(vector[indice], indice);
}