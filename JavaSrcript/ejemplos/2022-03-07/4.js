document.querySelector('button').addEventListener("click",()=>{
    // creo un fragmento de codigo
    let fragmento = document.createDocumentFragment();
    
    // creo una etiqueta div
    // let fragmento = document.createElement("div");

    for (let c = 1; c <= 3; c++) {
        //creo una etiqueta img
        let foto = document.createElement("img");
        // coloco el src de la imagen
        foto.src=`img/${c}.jpg`;
        // añado la foto al fragmento
        fragmento.appendChild(foto)
    }
    // añado al body el fragmento 
    document.querySelector('body').appendChild(fragmento);
});