window.addEventListener("load",()=>{
    // este codigo se ejecuta cuando se carge la web
    document.querySelector('button').addEventListener("click",()=>{
        // este codigo se ejecuta cuando pulso el boton
        
        // creo un div
        let caja = document.createElement("div");

        // creo un nodo de texto
        let texto = document.createTextNode("Hola mundo");

        document.querySelector('#salida').appendChild(caja);
        
        // añado texto al div
        //caja.innerHTML="Hola mundo";
        //caja.textContent="Hola mundo";
        caja.appendChild(texto);
    });
});