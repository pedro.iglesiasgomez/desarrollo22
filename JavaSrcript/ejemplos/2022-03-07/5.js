let textos = [
    "uno",
    "dos",
    "tres", [
        "cuatro",
        "sub1",
        "sub2"
    ]
];

textos.forEach(function(texto) {
    let elemento = document.createElement("li");
    let enlace = document.createElement("a");
    let menu = document.querySelector("#menu");

    if (texto instanceof Array) {
        let submenu = document.createElement("ul");
        enlace.innerText = texto.shift();
        enlace.href = "#";
        elemento.appendChild(enlace);
        texto.forEach(function(texto) {
            let subelemento = document.createElement("li");
            let subenlace = document.createElement("a");
            submenu.appendChild(subelemento);
            subenlace.innerText = texto;
            subenlace.href = "#";
            subelemento.appendChild(subenlace);
            submenu.appendChild(subelemento);
        });
        elemento.appendChild(submenu);
    } else {
        enlace.innerText = texto;
        enlace.href = "#";
        elemento.appendChild(enlace);
    }
    menu.appendChild(elemento);

});
