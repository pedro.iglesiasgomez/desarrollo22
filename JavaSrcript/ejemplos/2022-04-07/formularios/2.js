class Dato{
    constructor(numero=0, puerta="",direccion="",poblacion=""){
        this.numero=numero;
        this.puerta=puerta;
        this.direccion=direccion;
        this.poblacion=poblacion;
    }

    imprimir(etiqueta){
        let resultado="";
        resultado +=`
        <${etiqueta}>${this.numero}</${etiqueta}>
        <${etiqueta}>${this.puerta}</${etiqueta}>
        <${etiqueta}>${this.direccion}</${etiqueta}>
        <${etiqueta}>${this.poblacion}</${etiqueta}>`;

        return resultado;
    }

    static cabeceras(){
        let resultado="";
        let etiqueta= 'td';
        resultado +=
        `<${etiqueta}>id</${etiqueta}>
        <${etiqueta}>nombre</${etiqueta}>
        <${etiqueta}>edad</${etiqueta}>
        <${etiqueta}>poblacion</${etiqueta}>
        <${etiqueta}>email</${etiqueta}>`;
        
        return resultado;
    }
}
let datos=[];

/*function cabeceras(objeto){
    let resultado="";
    for (let titulo in objeto) {
        resultado+=`<td>${titulo}</td>`
    }
    return resultado
}*/

let cabeza = Dato.cabeceras();
document.querySelector('thead>tr').innerHTML=cabeza;

/*let cabeza = cabeceras(new Dato());
document.querySelector('thead>tr').innerHTML=cabeza;*/

document.querySelector('button').addEventListener("click",()=>{
    let localidad = new Dato(
        document.querySelector('#numero').value,
        document.querySelector('#puerta').value,
        document.querySelector('#direccion').value,
        document.querySelector('#poblacion').value,
    );

    /*document.querySelector('tbody').innerHTML+=`<tr>
    <td>${localidad.numero}</td>
    <td>${localidad.puerta}</td>
    <td>${localidad.direccion}</td>
    <td>${localidad.poblacion}</td></tr>`;*/

    datos.push(localidad)
    console.log(datos);

    document.querySelector('tbody').innerHTML+=`<tr>${localidad.imprimir('td')}</tr>`;
    document.querySelector('#salida').innerHTML+=`<ul>${localidad.imprimir('li')}</ul>`;
});