class Datos{
    constructor(id=0,nombre="",edad=0,poblacion="",email=""){
        this.id=id;        
        this.nombre=nombre;
        this.edad=edad;
        this.poblacion=poblacion;
        this.email=email;
    }

    imprimir(etiqueta){
        let resultado="";
        resultado +=
        `<${etiqueta}>${this.id}</${etiqueta}>
         <${etiqueta}>${this.nombre}</${etiqueta}>
         <${etiqueta}>${this.edad}</${etiqueta}>
         <${etiqueta}>${this.poblacion}</${etiqueta}>
         <${etiqueta}>${this.email}</${etiqueta}>`;

        return resultado;
    }

    static cabeceras(){
        let resultado="";
        let etiqueta= 'td';
       /* resultado +=
        `<${etiqueta}>id</${etiqueta}>
        <${etiqueta}>nombre</${etiqueta}>
        <${etiqueta}>edad</${etiqueta}>
        <${etiqueta}>poblacion</${etiqueta}>
        <${etiqueta}>email</${etiqueta}>`;
        
        return resultado;*/

        for (let titulo in new this()) {
            resultado += `<td>${titulo}</td>`
        }

        return resultado;
    }   
}

class ImprimirDatos{
    constructor(){
        this.registros=[];
    }

    imprimir(etiqueta="tr",etiqueta1="td",inicio=0,fin=this.registros.length){
        let resultado="";
        for(let indice = inicio;indice < fin;indice++){
            let registro = this.registros[indice].imprimir(etiqueta1);
            resultado +=`<${etiqueta}>${registro}</${etiqueta}>`;
        }
        return resultado;
    }
}

let personas= new ImprimirDatos();

let cabeza = Datos.cabeceras();
document.querySelector('thead>tr').innerHTML=cabeza;

document.querySelector('#mas').addEventListener("click",()=>{
    
    let dato = new Datos(
        document.querySelector('#id').value,
        document.querySelector('#nombre').value,
        document.querySelector('#edad').value,
        document.querySelector('#poblacion').value,
        document.querySelector('#email').value,
    );
    

    document.querySelector('tbody').innerHTML+=`<tr>${dato.imprimir('td')}</tr>`;
    
    personas.registros.push(dato);
    console.log(personas);


});

document.querySelector('#todos').addEventListener("click",function(e){
    document.querySelector('tbody').innerHTML= personas.imprimir();
    
});

document.querySelector('#cuatro').addEventListener("click",function(e){
    document.querySelector('tbody').innerHTML= personas.imprimir('tr','td',0,4);
    /*document.querySelector('tbody').innerHTML="";
    
    /*personas.forEach(function(dato,indice){
        if(indice < 4){
            document.querySelector('tbody').innerHTML+=`<tr>${dato.imprimir('td')}</tr>`;
        }        
    });

    for (let indice = 0; indice < 4; indice++ ) {
            let dato = personas[indice];

            document.querySelector('tbody').innerHTML+=`<tr>${dato.imprimir('td')}</tr>`;
    }*/
    
});

document.querySelector('#ocho').addEventListener("click",function(e){
    document.querySelector('tbody').innerHTML= personas.imprimir('tr','td',4,8);
});

document.querySelector('#resto').addEventListener("click",function(e){
    document.querySelector('tbody').innerHTML= personas.imprimir('tr','td',0);
    
});