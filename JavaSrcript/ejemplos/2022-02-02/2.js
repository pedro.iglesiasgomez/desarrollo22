/**
 * definicion de las variables
 */
var radio=0;
let perimetro=0;
var area=0;
let circulo=null; // apunta al div circulo

//const PI = 3.14;

/**
 * Introducir los datos
 */
radio=prompt("Introduce el radio");

/**
 * procesamiento de la informacion
 */

perimetro = 2*Math.PI*radio;

//area = Math.PI*(radio**2); // lo mismo pero con operador
area = Math.PI*Math.pow(radio,2);

/**
 * Mostrar resultados
 */

document.write("El radio es: " + radio + "<br>");
document.write("El perimetro es: " + perimetro + "<br>");
document.write("El area es: " + area);
//document.write(`El area es ${area}<br>`);

/**
 * dibujar circulo con radio radio
 */

circulo=document.querySelector("#circulo");
circulo.style.width=radio+"px";
circulo.style.height=radio+"px";
circulo.style.backgroundColor="grey";
circulo.style.borderRadius=radio + "px";