// variable que apunta al los elementos que quiero colocar el escuchador (onclick)
let cajas = document.querySelectorAll('div');

// recorro cada uno de los elemtos del array y les coloco el listener (onclick)
for (let caja of cajas) {
    caja.addEventListener('click', (e)=>{
        // e es el event que desencadena el evento
        
        // cambio el fondo del div sobre el que se pulsa
        e.target.style.backgroundColor="orange"

        // cambio el texto del div sobre el que se pulsa
        e.target.innerHTML=""
    })
}