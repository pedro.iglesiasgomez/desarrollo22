// declaracion e inicializacion de las variables
let uno=0;
let dos=0;
let suma=0;

// introduccion de datos
uno=10;
dos=20;

// procesamiento
// llamando a la funcion
suma=sumar(uno,dos);

//salida
console.log(suma);

//declarando la funcion
function sumar(numero1,numero2){
	return numero1+numero2;
}