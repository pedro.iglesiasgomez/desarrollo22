// me creo una variable para guardar el nombre
var nombre = "Pedro";

/**
 *  escribir directamente en el body
 */
document.write(nombre); 

/**
 * escribir directamente en la consola
 */ 
console.log(nombre); 

/**
 * escribir directamente en el div que tiene id "caja"
 */
document.querySelector("#caja").innerHTML=nombre

