class Caja{
    #texto="Hola";

    constructor(){
        this.borde=1;
        this.fondo="red";
    }

    get borde(){
        return this._borde;
    }

    set borde(valor){
        this._borde=valor;
    }

    get texto(){
        return this.#texto;
    }

    set texto(valor){
        this.#texto=valor;
    }

    get fondo(){
        return `backgroundColor=${this._fondo}`;
    }

    set fondo(valor){
        this._fondo=valor;
    }
}

const objeto = new Caja();
console.log(objeto.borde);