//Creando un array vacio
let vector=[];
//let vector=new Array(); Opcion 2, igual que la anterior

//Introducir con un bucle datos al vector
for(let contador=0;contador<10;contador++){
    vector[contador]=contador;
    //Esto es lo mismo que la linea anterior:
    //vector[0]=0
    //vector[1]=1
    //vector[2]=2
    //...
}
document.write(vector);

//Recorriendo el array para mostrarlo
for(let contador=0;contador<vector.length;contador++){
    document.write(`<div>${vector[contador]}<div>`);
}

/*
Vamos a crear un array con los li
*/
let elementos=document.querySelectorAll('li');
//querySelectorAll para que acceda a todos los elementos del array

for(let contador=0;contador<5;contador++){
    elementos[contador].innerHTML+= " " + contador;
    elementos[contador].style.backgroundColor="GRAY";//añadiendo CSS
    elementos[contador].style.border="2px solid red";//añadiendo CSS
    //Explica esto otro dia
    elementos[contador].addEventListener("click",
    function(){
        alert("hola");
    });
}
