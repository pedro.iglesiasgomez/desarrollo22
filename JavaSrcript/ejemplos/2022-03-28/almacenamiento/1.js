window.addEventListener("load",()=>{
    document.querySelector('div').innerHTML = localStorage.getItem("numeros");

    document.querySelector('#borra').addEventListener("click", () => {
        localStorage.removeItem("numeros");
        document.querySelector('div').innerHTML = localStorage.getItem("numeros");
    });

    document.querySelector("#mas").addEventListener("click",()=>{
        let num = document.querySelector('#numero').value;
        let numeros = num;
        let vectorNumeros=[];

        if(localStorage.getItem("numeros") != null){
            numeros = localStorage.getItem("numeros")+ "," + num;
        }
        localStorage.setItem("numeros", numeros);
        document.querySelector('div').innerHTML = numeros;

        // calculo la media
        vectorNumeros = numeros.split(",");
        let suma=0;
        let media=0;
        vectorNumeros.forEach(function(v){
            suma+= parseInt(v);
        });
        media=suma/vectorNumeros.length;
        document.querySelector('div').innerHTML += `<br>${media}`;
    });
})