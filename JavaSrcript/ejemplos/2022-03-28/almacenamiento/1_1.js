window.addEventListener("load",()=>{
    document.querySelector('div').innerHTML = localStorage.getItem("numeros");

    document.querySelector('#borra').addEventListener("click", () => {
        localStorage.removeItem("numeros");
        document.querySelector('div').innerHTML = localStorage.getItem("numeros");
    });

    document.querySelector("#mas").addEventListener("click",()=>{
        let num = document.querySelector('#numero').value;
        let numeros = [];


        if(localStorage.getItem("numeros") != null){
            numeros =JSON.parse(localStorage.getItem("numeros"));
        }

        numeros.push(num);

        localStorage.setItem("numeros",JSON.stringify(numeros));
        document.querySelector('div').innerHTML = numeros;

        // calculo la media
        let suma=0;
        let media=0;
        numeros.forEach(function(v){
            suma+= parseInt(v);
        });
        media=suma/numeros.length;
        document.querySelector('div').innerHTML += `<br>${media}`;
    });
})