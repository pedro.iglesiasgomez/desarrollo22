let datos=[];
function imprimir(datos){
    let resultado="";
    datos.forEach(e => {
        resultado+=`<li>${e.texto}</li>`
    });
    return resultado;
}
setInterval(()=>{
    fetch(
        'http://127.0.0.1/2022/desarrollo/ejemplos/rest/chat.php'
        )
        .then(response => {
            if(response.ok){
                return response.json();
            }else{
                console.log('error');
            }
             
        })
        .then(valores => {
            
            // recupero los datos del servidor
            
            datos = valores;
            
            
            // imprimo los datos
            document.querySelector('#salida').innerHTML = imprimir(datos);
        });    

},100);
document.querySelector('#nuevo').addEventListener("click", function(e) {
    const data = new FormData(document.querySelector('form'));

    fetch(
        'http://127.0.0.1/2022/desarrollo/ejemplos/rest/chat.php'
        , {
            method: 'post',
            body: data
        })
        .then(function(response) {
            if (response.ok) {
                return response.text()
            } else {
                throw "Error en la llamada Ajax";
            }

        })
        .then(function(texto) {
            console.log(texto);
        })
        .catch(function(err) {
            console.log(err);
        });

        fetch(
            'http://127.0.0.1/2022/desarrollo/ejemplos/rest/chat.php'
            )
            .then(response => {
                if(response.ok){
                    return response.json();
                }else{
                    console.log('error');
                }
                 
            })
            .then(valores => {
                
                // recupero los datos del servidor
                
                datos = valores;
                
                
                // imprimo los datos
                document.querySelector('#salida').innerHTML = imprimir(datos);
            });
});