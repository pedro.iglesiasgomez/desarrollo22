<?php
header("Access-Control-Allow-Origin: *");
try {
	//Creamos la conexión PDO por medio de una instancia de su clase
	$cnn = new PDO("mysql:host=127.0.0.1;dbname=dbs5897008","root","");

	//Preparamos la consulta sql
	$respuesta = $cnn->prepare("select * from usuarios");

	//Ejecutamos la consulta
	$respuesta->execute();

	//Creamos un array donde almacenaremos los datos
	
	//Recorremos array
	/*foreach($respuesta as $res){
	    $usuarios[]=$respuesta->fetch();
	}*/
	$usuarios=$respuesta->fetchAll();

	//Hacemos una impresion del array en formato JSON.
	echo json_encode($usuarios);

} catch (Exception $e) {

	echo $e->getMessage();
	
}