let numeros = [5,8];

if(numeros[0]<numeros[1]){
    console.log("numero1 no es mayor que numero2");
}

if(numeros[1]>0){
    console.log("numero2 es positivo");
}

if(numeros[0]<0 || numeros[0]!=0){
    console.log("numero1 es negativo o distinto de cero");
}

if((numeros[0]+1)<numeros[1]){
    console.log("Incrementar en 1 unidad el valor de numero1 no lo hace mayor o igual que numero2");
}