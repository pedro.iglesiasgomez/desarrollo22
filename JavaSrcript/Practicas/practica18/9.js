
var canvas;
var ctx;

window.addEventListener("load",inicio());

function inicio(){
    canvas=document.querySelector("canvas");
    ctx=canvas.getContext("2d");
    //dibujo1()
    //dibujo2();
    //dibujo3();
    //dibujo4();
    //dibujo5();
    //dibujo6();
    dibujo7();
};

function dibujo1(){
    ctx.fillRect(25,25,100,100);
    ctx.strokeRect(200,100,50,50);
}

function dibujo2(){
    ctx.fillRect(25,25,100,100);
    ctx.clearRect(45,45,60,60);
    ctx.strokeRect(50,50,50,50);
}

function dibujo3(){
    ctx.fillRect(5,5,100,100);
    ctx.fillRect(200,5,100,100);
    ctx.fillRect(5,200,100,100);
    ctx.fillRect(200,200,100,100);
}

function dibujo4(){
    ctx.fillStyle = "rgb(200,0,0)";
    ctx.fillRect(10,10,55,50);

    ctx.fillStyle = "rgb(0,0,200,0.5)";
    ctx.fillRect(30,30,55,50);
}


function dibujo5(){
    for(let x=10;x<600;x+=60){
        ctx.fillStyle = "rgb(200,0,0)";
        ctx.fillRect(10+x,10,55,50);

        ctx.fillStyle = "rgb(0,0,200,0.5)";
        ctx.fillRect(10+x,80,55,50);
    }
}

function dibujo6(){
    for(let x=10;x<600;x+=60){
        ctx.fillStyle = "rgb(200,0,0)";
        ctx.fillRect(10+x,10,55,50);

        ctx.strokeStyle = "rgb(0,0,200,0.5)";
        ctx.strokeRect(10+x,80,55,50);
    }
}

function dibujo7(){
    ctx.beginPath();
    ctx.arc(75,75,50,0,Math.PI*2,true); //circulo exterior

    ctx.moveTo(110,75);
    ctx.arc(75,75,35,0,Math.PI,false); // Boca

    ctx.moveTo(65,65);
    ctx.arc(60,65,5,0,Math.PI*2,true); // Ojo izquerdo

    ctx.moveTo(95,65);
    ctx.arc(90,65,5,0,Math.PI*2,true); // Ojo derecho

    ctx.stroke();
}



