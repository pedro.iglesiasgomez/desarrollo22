window.addEventListener("load",inicio);
let canvas;
let ctx;
let botones;

let imagen = new Image();
imagen.src="imgs/faceLogo.jpg";
imagen.style.width=25 + "px";

img.onload = function(){
    ctx.drawImage(img, 0, 0);
}

function inicio() {
    canvas=document.querySelector('canvas');
    ctx=canvas.getContext("2d");

    canvas.addEventListener("mousemove",function(e){
        ctx.lineTo(e.clientX-this.offsetLeft,e.clientY-this.offsetTop);
        ctx.stroke();
    })

    canvas.addEventListener("mouseover",function(e){
        ctx.beginPath();
        ctx.lineWidth=0.5; // cambia la altura de la linea
        ctx.moveTo(e.clientX-e.target.offsetLeft,e.clientY-e.target.offsetTop);
    })

    botones=document.querySelectorAll('div>ul>li');
    for (let c = 0; c < botones.length; c++) {
        botones[c].addEventListener("click",(e)=>{
            ctx.strokeStyle=e.target.getAttribute("data-color");
        });        
    }

    canvas.addEventListener("click",(e)=>{
        ctx.drawImage(imagen,e.clientX-e.target.offsetLeft,e.clientY-e.target.offsetTop);
    });
}