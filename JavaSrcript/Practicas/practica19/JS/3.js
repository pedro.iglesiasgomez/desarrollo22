window.addEventListener("load",inicio);
let canvas;
let ctx;
let botones;

function inicio() {
    canvas=document.querySelector('canvas');
    ctx=canvas.getContext("2d");

    canvas.addEventListener("mousemove",function(e){
        cuadrado(e.clientX-e.target.offsetLeft,e.clientY-e.target.offsetTop,10);
    })

    canvas.addEventListener("mouseover",function(e){
        ctx.beginPath();
        ctx.lineWidth=0.5; // cambia la altura de la linea
        ctx.moveTo(e.clientX-e.target.offsetLeft,e.clientY-e.target.offsetTop);
    })

    botones=document.querySelectorAll('div>ul>li');
    for (let c = 0; c < botones.length; c++) {
        botones[c].addEventListener("click",(e)=>{
            ctx.fillStyle=e.target.getAttribute("data-color");
        });
        
    }
}

function cuadrado(x,y,lado){
    ctx.fillRect(x,y,lado,lado);
}