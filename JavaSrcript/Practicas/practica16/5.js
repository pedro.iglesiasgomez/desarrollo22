// creamos una clase que recibe 3 argumentos

const Caja = function(a,b,c){
    // propiedad privada
    let unidad= "px"
    //propiedades publicas
    this.ancho=a;
    this.alto=b;
    this.texto=c;

    //metodo privado
    function concatenar(){
        this.ancho=this.ancho+unidad;
        this.alto=this.alto+unidad;
    }    
    // metodos publicos
    this.mensaje= function(){
        this.texto="esto es un mensaje";
    };
    this.mostrar=function(){
        alert(this.texto);
    };
}
