const Animal = function(){
    this.nombrePadre="soy el padre";
};

const Leon=function(){
    this.nombreHijo="soy el hijo";
};

// una forma de añadir los miembros de animal a Leon
Leon.prototype= new Animal();

const objetoPadre = new Animal();
const objetoHijo = new Leon();

console.log(objetoPadre.nombrePadre);
console.log(objetoHijo.nombrePadre);
console.log(objetoHijo.nombreHijo);

