// creamos una clase

const Persona = function(){
    // propiedad privada
    let nombre;
    // metodos publicos
    this.dormir=function() {
        console.log("zzzzzzzzzzz");
    };

    this.hablar=function(){
        console.log("BLA BLA BLA");
    };

    this.contar=function(){
        console.log("1 2 3 4 5 6 7");
    };
    this.setNombre=function(valor){
        nombre=valor;
    };

    this.getNombre=function(){
        return nombre;
    };
};

// crear un objeto con una instancia de la clase
const alumno= new Persona();

alumno.dormir();
alumno.hablar();
alumno.contar();
alumno.setNombre(prompt("Introduce el nombre"));
console.log(alumno.getNombre());