// creamos una clase padre

const Persona = function(){
    //propiedad privada
    let nombre;
    // metodos publicos
    this.dormir=function() {
        console.log("zzzzzzzzzzz");
    };

    this.hablar=function(){
        console.log("BLA BLA BLA");
    };

    this.contar=function(){
        console.log("1 2 3 4 5 6 7");
    };
    this.setNombre=function(valor){
        nombre=valor;
    };

    this.getNombre=function(){
       return nombre;
    };

};

// creamos una clase hija
const Hija =function(){
    this.atributoHijo=18;
};

// para que Hija herede todo lo de persona
Hija.prototype=new Persona();

// creamos un objeto desde la clase hija

const ana = new Hija();

// creamos un objetos desde la clase padre

const ramon = new Persona();

console.log(Ana.atributoHijo);
console.log(Ana);
console.log(Ramon);