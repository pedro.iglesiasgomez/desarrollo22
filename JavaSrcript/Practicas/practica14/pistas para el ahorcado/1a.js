function leer(){
	let vCaracter=document.querySelector("#caracter");
	let vPalabra=document.querySelector("#palabra");
	let vSalida=document.querySelector("#salida");
	let palabra=vPalabra.value;
	let caracter=vCaracter.value;
	let posiciones=[];
	let posicion=0;

	// busca la primera ocurrencia del caracter buscado
	posicion=palabra.search(caracter);
	while(posicion!=-1){
		posiciones.push(posicion)
		palabra=palabra.replace(caracter," ");
		posicion=palabra.search(caracter);
	}
	
	

	vSalida.innerHTML=posiciones.join(",");
}