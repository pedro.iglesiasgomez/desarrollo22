var frase = prompt("Escribe una frase y que no sea muy larga");
var frasemin=frase.toLowerCase();
var vector = frasemin.split("");
var contador=0;

for (let c = 0; c < vector.length; c++) {
    if (vector[c]=='a' ||
        vector[c]=='á' ||     
        vector[c]=='e' ||
        vector[c]=='é' ||    
        vector[c]=='i' ||
        vector[c]=='í' ||    
        vector[c]=='o' ||
        vector[c]=='ó' ||    
        vector[c]=='u' ||
        vector[c]=='ú'     
    ){
        contador++;
    }
}

document.write(`La frase o palabra ${frasemin}<br> tiene ${contador} vocales`)


