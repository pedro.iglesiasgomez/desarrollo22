
const Operaciones = function(num1,num2){
    this.num1=num1;
    this.num2=num2;
    this.resultado=0;
    this.sumar=()=>{
        this.resultado=this.num1+this.num2;
    };
    this.restar=()=>{
        this.resultado=this.num1-this.num2;
    };
    this.producto=()=>{
        this.resultado=this.num1*this.num2;
    };
    this.getresultado=()=>{
        return this.resultado;
    };
};


const operacion1 = new Operaciones(1,2);
operacion1.sumar();
console.log(operacion1.getresultado());
operacion1.restar();
console.log(operacion1.getresultado());




/*
const Operaciones = function(num1,num2){
    //miembros
    this.num1=0;
    this.num2=0;
    this.resultado=0;
    this.Sumar=()=>{
        this.resultado=this.num1+this.num2;
    };
    this.Restar=()=>{
        this.resultado=this.num1-this.num2;
    };
    this.Producto=()=>{
        this.resultado=this.num1*this.num2;
    };
    this.verResultado=()=>{
        console.log(this.resultado);
    };

    // metodo que se va a ejecutar nada mas crear el objeto
    this.Operaciones=function(num1,num2){
        this.num1=num1;
        this.num2=num2;
        this.resultado=0;
    };

    // llamo a la funcion constructora
    this.Operaciones(num1,num2);
};

const operacion1 = new Operaciones(1,2);
operacion1.Sumar();
operacion1.verResultado();
operacion1.Restar();
operacion1.verResultado();

*/

