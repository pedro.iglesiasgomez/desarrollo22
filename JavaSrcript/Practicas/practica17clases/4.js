var Operaciones = function(num1,num2){
    var sup = this;
    // miembros

    this.num1=0;
    this.num2=0;
    this.resultado=0;
    this.Sumar=()=>{
        this.resultado=this.num1+this.num2;
    };
    this.Restar=()=>{
        this.resultado=this.num1-this.num2;
    };
    this.Producto=()=>{
        this.resultado=this.num1*this.num2;
    };
    this.verResultado=()=>{
        console.log(this.resultado);
    };

    // metodo que se va a ejecutar nada mas crear el objeto
    function Operaciones(num1,num2){
        this.num1=num1;
        this.num2=num2;
        this.resultado=0;
    };

    // llamo a la funcion constructora
    Operaciones(num1,num2);
};

var operacion1 = Operaciones(1,2);
console.log(operacion1.num1);
operacion1.Sumar();
operacion1.verResultado();
operacion1.Restar();
operacion1.verResultado();