
class Operaciones{
    constructor(num1,num2){
        //miembros
        this.num1=num1;
        this.num2=num2;
        this._resultado=0;
        }
    sumar(){
        this.resultado=this.num1+this.num2;
    };
    restar(){
        this.resultado=this.num1-this.num2;
    };
    producto(){
        this.resultado=this.num1*this.num2;
    };
    get resultado(){
        return this._resultado + "cm";
    };
    set resultado(valor){
        this._resultado=isNaN(valor) ? 0 : valor;
    }
};


const operacion1 = new Operaciones(1,2);
operacion1.sumar();
console.log(operacion1.resultado);
operacion1.restar();
console.log(operacion1.resultado);
