class Animal{
    constructor(){
        this.peso=0;
    }

    static pelo="si";

    comer(){
        console.log("NAM");
    };

    tienePelo(){
        /* dentro de un metodo no estatico si podemos acceder a mienbros estaticos*/
        console.log("tiene pelo??" + Animal.pelo);
    };

    static estatico(){
        console.log("esto es un elemento de la clase");
        /* dentro de un elemento estatico no puedo llamar a elementos no estaticos
        this.comer()*/
        console.log(Animal.pelo);
    };
};

const leon = new Animal();
/* Produce error ya que es un metodo de la clase y no del objeto
leon.estatico();
*/
Animal.estatico();
console.log(Animal.pelo);
leon.tienePelo();
/*
Esto esta vacio porque es un elemento de la clase y no del objeto
console.log(leon.pelo)
*/ 