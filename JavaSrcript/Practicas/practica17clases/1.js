class Animal{
    constructor(){
    // prpiedad no estatica
        this.peso=0;
    }
    // propiedad estatica
    static pelo="si";
    
    //metodo no estatico
    comer(){
        console.log("ÑAM");
    };
    //metodo estatico
    static estatico(){
        console.log("esto es un elemento de la clase");
    };
};

const leon = new Animal();

/* Produce error ya que es un metodo de la clase y no del objeto 

    leon.estatico();

*/

Animal.estatico();
console.log(Animal.pelo);

/* Esto esta vacio porque es un elemento de la clase y no del objeto

    console.log(leon.pelo);

*/