// creamos una clase que recibe 3 argumentos

class Caja{
    // propiedad privada
    #unidad= "px"
    
    //metodo constructor
    constructor(ancho,alto,texto){
    //propiedades publicas
        this.ancho=ancho;
        this.alto=alto;
        this.texto=texto;
    }

    //metodo privado
    #concatenar(){
        this.ancho=this.ancho+this.#unidad;
        this.alto=this.alto+this.#unidad;
    }    
    // metodos publicos
    mensaje(){
        this.texto="esto es un mensaje";
    };
    mostrar(){
        alert(this.texto);
    };
}

// creamos el objeto

const objeto = new Caja(10,20,"hola mundo");

console.log(objeto.alto);
objeto.mostrar();