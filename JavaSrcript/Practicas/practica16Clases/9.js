class Animal{
    constructor(){
        this.nombrePadre="soy el padre";
    }
};

class Leon extends Animal{
    constructor(){
        super();
        this.nombreHijo="soy el hijo";
    }
};


const objetoPadre = new Animal();
const objetoHijo = new Leon();

console.log(objetoPadre.nombrePadre);
console.log(objetoHijo.nombrePadre);
console.log(objetoHijo.nombreHijo);

