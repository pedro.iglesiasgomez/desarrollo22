// crear un objeto con la clase Object

const persona = new Object();
persona.edad=10,
persona.nombre="Pepo";
persona.decirNombre=function(){
    console.log(this.nombre);
};

// accedemos a los elementos del objeto

console.log(persona.nombre);
persona.decirNombre;

/* 
     ESTO NO DE PUEDE

    hijo=new persona();
    hijo.decirNombre();
*/