// creamos una clase

class Persona{
    // propiedad privada
    #nombre="";
    // metodos publicos
    dormir(){
        console.log("zzzzzzzzzzz");
    };

    hablar(){
        console.log("BLA BLA BLA");
    };

    contar(){
        console.log("1 2 3 4 5 6 7");
    };

    get nombre(){
        return this.#nombre;
    }

    set nombre(valor){
        this.#nombre=valor;
    }
};

// crear un objeto con una instancia de la clase
const alumno= new Persona();

alumno.dormir();
alumno.hablar();
alumno.contar();
alumno.nombre=prompt("Introduce un nombre");
console.log(alumno.nombre);