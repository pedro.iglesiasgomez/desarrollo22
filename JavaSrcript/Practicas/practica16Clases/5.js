// creamos una clase que recibe 3 argumentos

class Caja{
    constructor(a,b,c){
        //propiedades publicas
        this.ancho=a;
        this.alto=b;
        this.texto=c;
    }
    // metodos publicos
    mensaje(){
        this.texto="esto es un mensaje";
    };
    mostrar(){
        alert(this.texto);
    };
}
