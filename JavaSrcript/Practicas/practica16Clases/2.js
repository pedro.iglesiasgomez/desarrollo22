// creamos una clase padre

class Persona{
    //propiedad privada
    #nombre="";

    // metodos publicos
    dormir() {
        console.log("zzzzzzzzzzz");
    };

    hablar(){
        console.log("BLA BLA BLA");
    };

    contar(){
        console.log("1 2 3 4 5 6 7");
    };
    set nombre(valor){
        this.#nombre=valor;
    };

    get nombre(){
        return this.#nombre;
    };

};

// creamos una clase hija
class Hija extends Persona{
    constructor(){
        super();
        this.atributoHijo=18;
    }
};

// creamos un objeto desde la clase hija

const ana = new Hija();

// creamos un objetos desde la clase padre

const ramon = new Persona();

console.log(Ana.atributoHijo);
console.log(Ana);
console.log(Ramon);